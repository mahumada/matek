import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-family: 'Oswald', sans-serif;
  }
  html {
    height: 100%;
  }
  body {
    color: ${props => props.theme.colors.contrast};
    background: ${props => props.theme.colors.background};
    height: 100%;
  }
  #__next {
  height: 100%;
  }
`
