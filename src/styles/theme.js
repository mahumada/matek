const theme = {
  colors: {
    // Branding
    primary: '#DBFF00',

    // Components
    contrast: '#FFFFFF',
    light: '#999999',
    soften: '#555555',
    shadow: '#2B2B2B',

    // Elements
    background: '#222222'
  },

  filters:{
    primary: 'brightness(0) saturate(100%) invert(96%) sepia(99%) saturate(7378%) hue-rotate(14deg) brightness(108%) contrast(105%)',
    contrast: 'brightness(0) saturate(100%) invert(100%) sepia(100%) saturate(0%) hue-rotate(305deg) brightness(102%) contrast(102%)',
    light: 'brightness(0) saturate(100%) invert(64%) sepia(0%) saturate(450%) hue-rotate(147deg) brightness(96%) contrast(85%)',
    soften: 'brightness(0) saturate(100%) invert(34%) sepia(18%) saturate(0%) hue-rotate(213deg) brightness(89%) contrast(91%)',
    shadow: 'brightness(0) saturate(100%) invert(15%) sepia(0%) saturate(112%) hue-rotate(203deg) brightness(89%) contrast(90%)',
    background: 'brightness(0) saturate(100%) invert(12%) sepia(0%) saturate(12%) hue-rotate(153deg) brightness(92%) contrast(94%)'
  },

  sizes: {
    fullscreen: '25vw',
    big: '36px',
    medium: '24px',
    small: '12px',
    smaller: '9px',
    tiny: '6px',

    dinamic: {
      halfscreen: '9vw',
      bigger: '5vw',
      big: '3vw',
      medium: '2vw',
      small: '1vw',
      smaller: '.8vw',
      tiny: '.6vw',
      tiniest: '.3vw',
    }
  },

  timings: {
    hover: '.4s',
  }
}

export default theme;