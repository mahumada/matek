import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 100vh;
  overflow: hidden;
  scroll-snap-align: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: relative;
`

export const Text = styled.p`
  font-size: ${props => props.theme.sizes.dinamic.halfscreen};
  line-height: ${props => `calc(${props.theme.sizes.dinamic.halfscreen} + ${props.theme.sizes.dinamic.medium})`};
  font-weight: 700;
  color: ${props => props.colored ? props.theme.colors.primary : props.theme.colors.text};
  margin: 0;
`