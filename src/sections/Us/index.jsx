import React from 'react';
import { Container, Text } from './styles';

const Us = () => {
  return (
    <Container>
      <Text>WE TURN</Text>
      <Text colored>IDEAS</Text>
      <Text>INTO REALITY</Text>
    </Container>
  )
}

export default Us;