import React from 'react';
import { Container } from './styles';
import TitleName from '../../components/Map/TitleName';
import MapOutline from '../../components/Map/MapOutline';
import Controls from '../../components/Map/Controls';
import ArrowDown from '../../components/common/ArrowDown';
import TitleDescription from '../../components/Map/TitleDescription';
import SpiderWebBG from '../../components/Map/SpiderWebBG';

const Map = () => {
  return (
    <Container>
      {/* <MapOutline /> */}
      <SpiderWebBG />
      <TitleName>MATEK</TitleName>
      <TitleDescription>WEBAPPS</TitleDescription>
      <Controls />
      <ArrowDown />
    </Container>
  )
}

export default Map;