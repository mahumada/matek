import React from 'react';
import { Container, Input, SubmitButton, Text } from './styles';

const Contact = () => {
  return (
    <Container>
      <Text>LET'S CHAT</Text>
      <Input placeholder='INSERT YOUR EMAIL HERE' />
      <Input placeholder='INSERT YOUR MESSAGE HERE' />
      <SubmitButton>SEND</SubmitButton>
    </Container>
  )
}

export default Contact;