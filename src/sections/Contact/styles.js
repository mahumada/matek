import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 100vh;
  overflow: hidden;
  scroll-snap-align: center;
  display: flex;
  flex-direction: column;
  position: relative;
  padding: 2%;
`

export const Text = styled.p`
  font-size: ${props => props.theme.sizes.dinamic.halfscreen};
  line-height: ${props => `calc(${props.theme.sizes.dinamic.halfscreen} + ${props.theme.sizes.dinamic.medium})`};
  font-weight: 700;
  color: ${props => props.colored ? props.theme.colors.primary : props.theme.colors.text};
  margin: 0;
`

export const Input = styled.input`
  font-size: ${props => props.theme.sizes.dinamic.bigger};
  line-height: ${props => `calc(${props.theme.sizes.dinamic.bigger} + ${props.theme.sizes.dinamic.medium})`};
  font-weight: 500;
  border: none;
  background: none;
  color: ${props => props.theme.colors.soften};
  margin: 0;
  padding: ${props => props.theme.sizes.dinamic.tiny};
  outline: none;
`

export const SubmitButton = styled.button`
  font-size: ${props => props.theme.sizes.dinamic.bigger};
  line-height: ${props => `calc(${props.theme.sizes.dinamic.bigger} + ${props.theme.sizes.dinamic.medium})`};
  font-weight: 700;
  border: none;
  background: none;
  color: ${props => props.theme.colors.primary};
  position: absolute;
  bottom: 2%;
  left: 2%;
  margin: 0;
`