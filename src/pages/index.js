// Import Components
import Layout from '../components/Layout';
import { Map } from '../sections';
import Contact from '../sections/Contact';
import Us from '../sections/Us';

// Import Sections

export default function Home() {
  return (
      <Layout>
        <Map />
        <Us />
        <Contact />
      </Layout>
  )
}
