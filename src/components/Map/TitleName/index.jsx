import React from 'react';
import { Text } from './styles';

const TitleName = ({ children }) => {
  return <Text>{children}</Text>
}

export default TitleName;