import styled from 'styled-components';

export const Text = styled.h1`
  font-size: ${props => props.theme.sizes.fullscreen};
  height: ${props => props.theme.sizes.fullscreen};
  line-height: ${props => props.theme.sizes.fullscreen};
  font-weight: 700;
  color: ${props => props.theme.colors.shadow};
  -webkit-text-stroke: ${props => props.theme.sizes.dinamic.tiniest} ${props => props.theme.colors.contrast};
  text-shadow: 0 0 ${props => props.theme.sizes.dinamic.medium} black;
  z-index: 3;
`