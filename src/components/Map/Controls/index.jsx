import { ControlBtn, Icon, LanguageBox, LanguageText, Wrapper } from "./styles";
import { useState } from 'react';

const Controls = () => {

  const [langBoxDisplay, setLangBoxDisplay] = useState(false);
  const [fullscreen, setFullscreen] = useState(false);
  const [audio, setAudio] = useState(false);


  return (
    <Wrapper>
      <ControlBtn onClick={() => setFullscreen(!fullscreen)}>
        <Icon src={`/assets/icons/fullscreen${fullscreen ? 'Off' : 'On'}.png`} />
      </ControlBtn>
      <ControlBtn onClick={() => setAudio(!audio)}>
        <Icon src={`/assets/icons/sound${audio ? 'Off' : 'On'}.png`} />
      </ControlBtn>
      <LanguageBox display={langBoxDisplay}>
        <ControlBtn onClick={() => setLangBoxDisplay(!langBoxDisplay)} active={langBoxDisplay}>
          <Icon src="/assets/icons/language.png" />
        </ControlBtn>
        <LanguageText display={langBoxDisplay}>ENGLISH</LanguageText>
      </LanguageBox>
    </Wrapper>
  )
}

export default Controls;