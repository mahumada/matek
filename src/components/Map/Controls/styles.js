import styled from "styled-components";

export const Wrapper = styled.div`
  position: absolute;
  bottom: ${props => props.theme.sizes.small};
  left: ${props => props.theme.sizes.small};

  display: flex;
  align-items: center;
  gap: ${props => props.theme.sizes.medium};
  z-index: 5;
`

export const LanguageBox = styled.div`
  display: flex;
  gap: ${props => props.theme.sizes.smaller};
  align-items: center;

  background: ${props => props.display ? props.theme.colors.shadow : 'none'};
  border-radius: ${props => props.theme.sizes.tiny};
  padding:  0 ${props => props.theme.sizes.tiny};

  transition: ${props => props.theme.timings.hover};
`

export const LanguageText = styled.p`
  color: ${props => props.theme.colors.light};
  font-size: ${props => props.theme.sizes.medium};
  font-weight: 500;
  text-align: center;
  opacity: ${props => props.display ? 1 : 0};
  
  width: ${props => props.display ? '86px' : 0};
  overflow: hidden;

  transition: ${props => props.theme.timings.hover};
`

export const ControlBtn = styled.button`
  width: ${props => props.theme.sizes.medium};
  height: ${props => props.theme.sizes.medium};
  margin: ${props => props.theme.sizes.tiny} 0;

  background: none;
  border: none;

  transition: ${props => props.theme.timings.hover};
  filter: ${props => props.active ? props.theme.filters.light : props.theme.filters.soften};

  &:hover {
    filter: ${props => props.theme.filters.light};
    transform: scale(1.1);
  }
`

export const Icon = styled.img`
  width: 100%;
  height: 100%;
`