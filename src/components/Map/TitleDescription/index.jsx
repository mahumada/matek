import React from 'react';
import { Text } from './styles';

const TitleDescription = ({ children }) => {
  return <Text>{children}</Text>
}

export default TitleDescription;