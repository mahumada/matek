import styled from 'styled-components';

export const Text = styled.h1`
  font-size: ${props => props.theme.sizes.dinamic.bigger};
  font-weight: 700;
  margin: 0;
  width: 67%;
  height: ${props => props.theme.sizes.dinamic.bigger};
  color: ${props => props.theme.colors.text};
  text-align: end;
  z-index: 3;
`