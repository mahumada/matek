import React from 'react';
import { Map } from './styles';

const MapOutline = () => {
  return <Map src="/assets/mapOutline.png" />
}

export default MapOutline;