import { Wrapper, Icon } from "./styles";

const ArrowDown = () => {
  return (
    <Wrapper>
      <Icon src="/assets/icons/arrowDown.png" />
    </Wrapper>
  )
}

export default ArrowDown;