import styled from "styled-components";

export const Wrapper = styled.div`
  width: ${props => props.theme.sizes.medium};
  height: ${props => props.theme.sizes.small};
  
  position: absolute;
  bottom: ${props => props.theme.sizes.medium};
  left: ${props => 'calc(50% - ' + props.theme.sizes.medium + ' / 2)'};
  
  filter: ${props => props.theme.filters.soften};
`

export const Icon = styled.img`
  width: 100%;
  height: 100%;
`